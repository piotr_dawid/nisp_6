#include "student_b.h"

int IsOdd(int digit)
{
    if(digit % 2 == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int min(int digits[], int size)
{
    int min = digits[0];
    for(int i = 0 ; i < size ; i++)
    {
        if(min > digits[i])
        {
            min = digits[i];
        }
    }
    return min;
}
