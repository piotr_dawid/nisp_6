#include "student-c.h"

int isPrime(int number)
{
    if(number <= 1)
    {
        return 0;
    }
    
    for(int i = 2; i < number; i++)
    {
        if(number%i == 0)
        {
            return 0;
        }
    }

    return 1;
}

int average(int digits[], int size)
{
    int avg = 0;

    for(int i = 0; i < size; i++)
    {
        avg += digits[i];
    }

    avg /= size;
    return avg;
}